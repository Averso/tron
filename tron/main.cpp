/*
1. zalaczaenie SDL'a: https://www.youtube.com/watch?v=I-yOf4Xg_R8
-on wrzuca dll-ke do debuga, a powino sie tam gdzie main
-sdl image załączamy analogicznie, wrzucamy folder, ustawiamy miejsce includow, libow, tak gdzie sdl2.lib itp wpisywal
wpisujemy sdl_im...lib *(to z folderu x86), i po buildzie kopiujemy wszystkie dll to folderu z mainem
2. http://lazyfoo.net/tutorials/SDL/

*/

#include <SDL.h>
#include <stdio.h>
#include <string>

#include "Defines.h"
#include "Texture.h"
#include "Player.h"
#include "Board.h"
#include "Timer.h"




bool init();		//inicjalizacja - sdl,okno itp
bool loadFiles();	//wczytanie plikow - tekstury itp
void close();		//zwolnienie powierzchni, pamieci itp.


SDL_Window* window = NULL;	//wskaznik na okno
SDL_Renderer* winRenderer = NULL;	//tzw renderer - on rysuje w oknie

Player *player1 = new Player(1, 0, 0);	//gracz 1



double coun = 0.0;
int direction = 1; //prawo - 1, góra - 2, lewo - 3, dół - 4 


void renderTrails()
{

}

bool fun(Player & player, Board & board){
	//coun += 1;
	//double ww = coun - (int)coun;
	//int tmp = ww * 10;
//	if (tmp == 0){
		player.newPosition();
		if (!board.set(player.getX(), player.getY(), player.getId()))
			return false;
//	}

		
		return true;
}


int main(int argc, char* args[])
{

	Board board;	//obiekt planszy


	if (!init())
	{
		printf("Problem w inicjalizacji\n");
	}
	else
	{
		if (!loadFiles())
		{
			printf("Problem z wczytaniem plikow\n");
		}
		else
		{
			
			bool quit = false;
			SDL_Event e;	//do obslugi eventow
			

			//PETLA GLOWNA
			while (!quit)
			{
				
					while (SDL_PollEvent(&e) != 0)
					{
						
						if (e.type == SDL_QUIT)
						{
							quit = true;
						}
						else if (e.type == SDL_KEYDOWN) //obsluga klawiatury
						{
							switch (e.key.keysym.sym)
							{
							case SDLK_UP:
								player1->setDirectionX(0);
								player1->setDirectionY(-1);
								break;

							case SDLK_DOWN:
								player1->setDirectionX(0);
								player1->setDirectionY(1);
								break;

							case SDLK_LEFT:
								player1->setDirectionX(-1);
								player1->setDirectionY(0);
								break;

							case SDLK_RIGHT:
								player1->setDirectionX(1);
								player1->setDirectionY(0);

								break;
							default:
								break;

							}
						}
					}

					//Czyszczenie ekranu
					SDL_SetRenderDrawColor(winRenderer, 0x11, 0x01, 0x11, 0xFF);
					SDL_RenderClear(winRenderer);

					//renderowanie gracze i sladow
					

					//ruszanie gracza
					if (!fun(*player1, board))
					{
						break;
					}

					
					board.renderTrails(player1, winRenderer);
					player1->renderVehicle(winRenderer, 0);
					
					//Update screen
					SDL_RenderPresent(winRenderer);

				}
			}
		}

		//Free resources and close SDL
		close();

		return 0;
}


bool init()
{
	//właczenie sdla
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Blad inicjalizacji SDL: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		//wlaczenie filtrowania liniowego - do tekstu
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Filtrowanie liniowe NIE wlaczane.");
		}

		//Tworzenie okna
		window = SDL_CreateWindow("TRON", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Problem ze stworzeniem okna SDL Error: %s\n", SDL_GetError());
			return false;
		}
		else
		{
			//renderer dla okna z v sync
			winRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (winRenderer == NULL)
			{
				printf("Problem z stworzeniem renderera: %s\n", SDL_GetError());
				return  false;
			}
			else
			{
				//Kolor tla
				SDL_SetRenderDrawColor(winRenderer, 0xFF, 0x11, 0x01, 0x11);

				//WAZNE - skala - jezeli zmienimy rozmiar okna to renderer bedzie odpowiednio
				//skalowal wszystko co rysuje
				SDL_RenderSetScale(winRenderer, SCREEN_WIDTH / 640.0, SCREEN_HEIGHT / 480.0);

				//mozliwosc wczytywania plikow png
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("Problem z inicjalizacja SDL_Image: %s\n", IMG_GetError());
					return false;
				}
			}
		}
	}

	return true;
}


bool loadFiles()
{
	//tekstura pojazdu gracza
	if (!player1->loadVehTexture("spr/player1.png", winRenderer))
	{

		printf("Problem z wczytaniem tekstury \"player1.png\".\n");
		return false;
	}
	//tekstura 
	if (!player1->loadTrailTexture("spr/player1trail.png", winRenderer))
	{
		printf("Problem z wczytaniem tekstury \"player1trail.png\".\n");
		return false;
	}

	return true;
}


void close()
{
	//Free loaded images
	player1->freeTextures();

	//Destruktor okna i renderera
	SDL_DestroyRenderer(winRenderer);
	SDL_DestroyWindow(window);
	window = NULL;
	winRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}