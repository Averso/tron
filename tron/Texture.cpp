#include "Texture.h"

Texture::Texture()
{
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

bool Texture::loadFromFile(std::string path,SDL_Renderer* winRenderer)
{
	//usu� poprzednia
	free();

	SDL_Texture* newTexture = NULL;

	//wcyztaj powierzchnie ze �cie�ki
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());

	if (loadedSurface == NULL)
	{
		printf("Nie mo�na wczytac pliku %s\n", path.c_str());
	}
	else
	{
		//Klucz na kolor, ale wczytujemy png wiec nei trzeba
		//SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0x00, 0x90));

		//tworzenie tekstury z powierzchni
		newTexture = SDL_CreateTextureFromSurface(winRenderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Problem z stworzeniem tekstury %s!", path.c_str());
		}
		else
		{
			//wczytaj wymiary
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//zwolnij powierzchnie
		SDL_FreeSurface(loadedSurface);
	}

	mTexture = newTexture;

	//jak niezerowa to sie udalo
	if (mTexture != NULL)
		return true;
	else
		return false;
}

void Texture::free()
{
	//jak istnieje to zwolnij
	if (mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}


void Texture::render(SDL_Rect* rect, SDL_Renderer* winRenderer,double angle)
{
	SDL_RenderCopyEx(winRenderer, mTexture, NULL, rect, angle, NULL, SDL_FLIP_NONE); //NULL, bo to jest �r�owy rect, ale my piszemy do recta playera
}


int Texture::getWidth()
{
	return mWidth;
}

int Texture::getHeight()
{
	return mHeight;
}