#ifndef Texture_h
#define Texture_h

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>


class Texture
{
public:
	
	Texture();

	//Gey
	int getWidth();
	int getHeight();

	//metody	
	void free();	//zwolnienie powierzchni
	bool loadFromFile(std::string path, SDL_Renderer* winRenderer);				//wczytanie z pliku
	void render(SDL_Rect* rect, SDL_Renderer* winRenderer, double angle);		//renderowanie 

	
private:
	
	int mWidth;
	int mHeight;
	SDL_Texture* mTexture;
	
};

#endif